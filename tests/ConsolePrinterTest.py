import unittest
from unittest import mock

from pyprint.ConsolePrinter import ConsolePrinter
from pyprint.ContextManagers import retrieve_stdout, replace_stdout


class ConsolePrinterTest(unittest.TestCase):
    def setUpClass():
        assert ConsolePrinter.colorama_initialized is False

    def test_printing(self):
        self.uut = ConsolePrinter(print_colored=True)
        self.assertEqual(ConsolePrinter.colorama_initialized, True)

        with retrieve_stdout() as stdout:
            self.uut.print("\ntest", "message", color="green")
            self.assertRegex(stdout.getvalue(), "\033.*\ntest message.*")

        with retrieve_stdout() as stdout:
            self.uut.print("\ntest", "message", color="greeeeen")
            self.assertEqual(stdout.getvalue(), "\ntest message\n")

        with retrieve_stdout() as stdout:
            self.uut.print("\ntest", "message")
            self.assertEqual(stdout.getvalue(), "\ntest message\n")

        self.uut = ConsolePrinter()
        self.assertEqual(ConsolePrinter.colorama_initialized, True)

    @unittest.mock.patch('io.StringIO', autospec=True)
    def test_charset(self, stringio_mock):
        self.uut = ConsolePrinter(print_colored=True)

        mock_io_instance = stringio_mock()
        mock_io_instance.encoding = 'Windows-1250'
        with replace_stdout(mock_io_instance):
            self.uut.print("č")
            print_call = bytes('č', 'utf-8').decode('Windows-1250') + "\n"
            mock_io_instance.write.assert_any_call(print_call)

        mock_io_instance = stringio_mock()
        mock_io_instance.encoding = 'Windows-1250'
        with replace_stdout(mock_io_instance):
            self.uut.print("č", color="green")
            print_call = "\x1b[32m{0}\n\x1b[0m".format(
                bytes('č', 'utf-8').decode('Windows-1250'))
            mock_io_instance.write.assert_any_call(print_call)
